#
# This is a project Makefile. It is assumed the directory this Makefile resides in is a
# project subdirectory.
#

PROJECT_NAME := gauntled
ADD_INCLUDEDIRS := main include lib/ESP32_LED_STRIP/components/led_strip/inc

include $(IDF_PATH)/make/project.mk

