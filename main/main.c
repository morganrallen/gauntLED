#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "freertos/queue.h"
#include "driver/gpio.h"
#include "nvs_flash.h"
#include "esp_log.h"

#include "network.h"
#include "gauntlet.h"

#define ADC_CHANNEL           (ADC_CHANNEL_4) // GPIO 32

#define ESP_INTR_FLAG_DEFAULT 0
#define DEFAULT_VREF          1100
#define NO_OF_SAMPLES         64

#define ADC_MIN               ((uint32_t)162) // these two are observed values
#define ADC_MAX               ((uint32_t)3139)
#define MAX_RUN_TIME          ((uint32_t)10000) // 10 seconds
#define MIN_RUN_TIME          ((uint32_t)1000) // 10 seconds

#define TAG                   "ATMOS"

void app_main() {
  esp_err_t ret;

  // Initialize NVS.
  ret = nvs_flash_init();
  if (ret == ESP_ERR_NVS_NO_FREE_PAGES) {
    ESP_LOGI(TAG, "Erasing flash memory");
    ESP_ERROR_CHECK(nvs_flash_erase());
    ret = nvs_flash_init();
  }
  ESP_ERROR_CHECK( ret );

  network_init();

  gauntlet_init();

  while(1) {
    vTaskDelay(1000 / portTICK_RATE_MS);
  }
}
