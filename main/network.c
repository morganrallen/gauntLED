#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "freertos/queue.h"
#include "esp_log.h"

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include <lwip/netdb.h>

#include "esp32-wifi-manager.h"

#include "esp32-wifi-manager.h"

#define TAG "ATMOFI"

EventGroupHandle_t wm_event_group;

void network_init() {
  ESP_LOGI(TAG, "initializing WiFi");
  wm_event_group = wifi_manager_start();
  wifi_manager_reset_store();

  if(wifi_manager_ap_count() == 0) {
    ESP_LOGI(TAG, "Adding new AP");

    if(strlen(CONFIG_WIFI_MANAGER_TEST_AP) > 0) {
      if(strlen(CONFIG_WIFI_MANAGER_TEST_USER) > 0)
        wifi_manager_add_peap_ap(CONFIG_WIFI_MANAGER_TEST_AP, CONFIG_WIFI_MANAGER_TEST_USER, CONFIG_WIFI_MANAGER_TEST_PWD);
      else
        wifi_manager_add_ap(CONFIG_WIFI_MANAGER_TEST_AP, CONFIG_WIFI_MANAGER_TEST_PWD);
    }
  }

  wifi_manager_connect();
}
