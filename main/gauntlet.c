#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/timers.h"
#include "freertos/event_groups.h"
#include "esp_log.h"
#include "led_strip.h"

#include "gauntlet.h"

#define TAG                   "GAUNTLET"

#define PIN_BTN1              (25)
#define PIN_BTN2              (32)
#define PIN_LED               (12)
#define LED_COUNT             (128L)
#define LED_STRIP_RMT_INTR_NUM 19U

#define BUTTON_DEBOUNCE       (50)

#define COUNT_TIME_MS         (100)

#define GPIO_INPUT_PIN_SEL    ((1ULL<<PIN_BTN1) | (1ULL<<PIN_BTN2))

#define PLAYER_COLOR          0, 255, 0

#define MARCH_SPEED           100

#define MAX_ENEMIES           (12)
#define MAX_HOLD              ((float)3000)
#define LED_HOLD              (3)

#define MAX_DASH              ((float)3000)
#define LED_DASH              (4)

enum ENEMY_MODE {
  ADVANCE, // just moves forward
  PATROL, // moves forward and returns
  POUNCE, // advances when player approaches
  RUNNER // will retreat if player holds
};

struct player {
  uint8_t dashing; // is the player presently dashing
  uint8_t holding; // is the player presently holding
  uint8_t lives;
  uint32_t dash;
  uint32_t hold;
  uint32_t pos;
} player;

typedef struct enemy {
  enum ENEMY_MODE mode;
  uint8_t origin; // starting place
  uint8_t pos; // current position
  uint8_t dir; // which direction is it currently going
  uint8_t dist; // distance a PATROL will travel
} enemy;

struct enemy enemies[MAX_ENEMIES];
uint8_t enemy_count = 0;

//static uint32_t time_to_hold = 3000;
//static uint32_t dash_recover_time = 1000;

static EventGroupHandle_t count_group;
static TimerHandle_t count_timer = NULL;

enum STATE {
  STARTUP,
  LEVELUP,
  RUN,
  DEATH
};

uint8_t level = 0;
uint8_t state = STARTUP;
uint8_t count = 0;
uint32_t count_time = 0;

static xQueueHandle gpio_evt_queue = NULL;

void set_pixel_rgb(struct led_strip_t *led_strip, uint32_t pos, uint8_t r, uint8_t g, uint8_t b) {
  led_strip_set_pixel_rgb(led_strip, pos + 5, r, g, b);
}

void button_loop(void *arg) {
  static uint32_t io_num;
  static uint8_t io_level;

  int8_t last_dash = -1;
  int8_t last_hold = -1;

  //uint32_t dash_down = 0;

  while(1) {
    if(xQueueReceive(gpio_evt_queue, &io_num, portMAX_DELAY)) {
      io_level = !gpio_get_level(io_num);

      if(io_num == PIN_BTN1 && (io_level != last_hold || last_hold == -1)) {
        printf("GPIO[%d] intr, val: %d\n", io_num, io_level);
        player.holding = io_level;
        last_hold = io_level;
      } else if(io_num == PIN_BTN2 && (io_level != last_dash || last_dash == -1)) {
        if(io_level == 1) {
          //dash_down = esp_timer_get_time() / 1000ULL;

          player.dashing = io_level;
        } else {
          //ESP_LOGI(TAG, "DASH! %lld", (esp_timer_get_time() / 1000ULL) - dash_down);
          // player.pos += 5;
        }

        last_dash = io_level;
      }
    }
  }
}

void gauntlet_loop(void *arg) {
  struct led_strip_t *led_strip = arg;
  unsigned long last_millis = 0;
  unsigned long millis;
  unsigned long diff;

  while(1) {
    switch(state) {
      case STARTUP:
        level = 0;

        player.pos = 0;
        player.lives = 3;
        player.dashing = 0;
        player.holding = 0;

        player.dash = 0;
        player.hold = MAX_HOLD;

        state = LEVELUP;
      break;

      case LEVELUP:
        ++level;

        ESP_LOGI(TAG, "Starting level %d", level);

        player.pos = 0;

        player.dashing = 0;
        player.holding = 0;

        player.dash = 0;
        player.hold = MAX_HOLD;

        switch(level) {
          case 4:
            enemy_count = 1;

            enemies[0].mode = RUNNER;
            enemies[0].pos = LED_COUNT - LED_COUNT / 4; // 3/4 down the strip
            enemies[0].origin = LED_COUNT - LED_COUNT / 4; // 3/4 down the strip
          break;

          case 3:
            enemy_count = 2;

            enemies[0].mode = PATROL;
            enemies[0].origin = LED_COUNT / 4;
            enemies[0].pos = LED_COUNT / 4;
            enemies[0].dist = 10;

            enemies[1].mode = PATROL;
            enemies[1].origin = LED_COUNT / 4 * 2;
            enemies[1].pos = LED_COUNT / 4 * 2;
            enemies[1].dist = 10;
          break;

          case 2: {
            enemy_count = 1;

            enemies[0].mode = PATROL;
            enemies[0].origin = LED_COUNT / 2;
            enemies[0].pos = LED_COUNT / 2;
            enemies[0].dist = 10;
          break; }

          default:
            enemy_count = 1;

            enemies[0].mode = ADVANCE;
            enemies[0].origin = LED_COUNT - 5;
            enemies[0].pos = LED_COUNT - 5;
        }

        state = RUN;
      break;

      case DEATH:
        player.lives--;

        ESP_LOGI(TAG, "You dead. %d lives left", player.lives);

        set_pixel_rgb(led_strip, player.pos - 2,  55, 0, 0);
        set_pixel_rgb(led_strip, player.pos - 1, 127, 0, 0);
        set_pixel_rgb(led_strip, player.pos,     255, 0, 0);
        set_pixel_rgb(led_strip, player.pos + 1, 127, 0, 0);
        set_pixel_rgb(led_strip, player.pos + 2,  55, 0, 0);

        vTaskDelay(10 / portTICK_RATE_MS);
        led_strip_show(led_strip);

        vTaskDelay(3000 / portTICK_RATE_MS);

        // decrement level to 'level up' to the same one
        if(level > 0) {
          --level;
        }

        // decrement lives and check if we should restart the level or game
        if(player.lives > 0) {
          state = LEVELUP;
        } else {
          state = STARTUP;
        }
      break;

      case RUN:
        millis = esp_timer_get_time() / 1000ULL;
        uint8_t dash_tracer = 5;
        diff = millis - last_millis;

        for(uint8_t i = 0; i < player.lives; i++) {
          led_strip_set_pixel_rgb(led_strip, i, 255, 0, 0);
        }

        if(diff > MARCH_SPEED) {
          if(player.dashing) {
            player.pos += dash_tracer;
            player.dashing = 0;
            player.dash = 0;

            for(uint8_t i = 0; i < dash_tracer; i++) {
              //ESP_LOGI(TAG, "tracer: %d", (uint8_t)(((float)i / dash_tracer) * 255));
              set_pixel_rgb(led_strip, player.pos - dash_tracer + i, 0, (uint8_t)(((float)i / dash_tracer) * 255), 0);
            }
          } else if(!player.holding) {
            player.pos++;

            if(player.hold < MAX_HOLD) {
              player.hold += diff;
            }
          } else if(player.holding) {
            if((int32_t)(player.hold - diff) < 0) {
              player.holding = 0;
            } else {
              player.hold -= diff;
            }
          }

          if(player.dash < MAX_DASH) {
            player.dash += diff;
          }

          led_strip_set_pixel_rgb(led_strip, LED_HOLD, 0, 0, (uint8_t)(((float)player.hold / MAX_HOLD) * 255));
          led_strip_set_pixel_rgb(led_strip, LED_DASH, 0, 0, (uint8_t)(((float)player.dash / MAX_DASH) * 255));

          set_pixel_rgb(led_strip, player.pos, PLAYER_COLOR);

          for(uint8_t i = 0; i < enemy_count; i++) {
            if(enemies[i].mode == ADVANCE) {
              enemies[i].pos--;
            } else if(enemies[i].mode == POUNCE) {
            } else if(enemies[i].mode == RUNNER) {
              if(player.holding && enemies[i].pos < LED_COUNT - 5) {
                enemies[i].pos++;
              } else {
                enemies[i].pos--;
              }
            } else if(enemies[i].mode == PATROL) {
              int8_t dist = enemies[i].origin - enemies[i].pos;

              if(abs(dist) >= enemies[i].dist) { // has traveled total distance
                enemies[i].dir = !enemies[i].dir; // change direction
              }

              if(enemies[i].dir) {
                enemies[i].pos++;
              } else {
                enemies[i].pos--;
              }
            }

            set_pixel_rgb(led_strip, enemies[i].pos, 255, 0, 0);

            if(!player.dashing && (enemies[i].pos == player.pos || enemies[i].pos + 1 == player.pos)) {
              state = DEATH;

              led_strip_show(led_strip);

              break;
            }
          }

          last_millis = millis;

          led_strip_show(led_strip);
        }

        if(player.pos >= LED_COUNT - 5) {
          ESP_LOGI(TAG, "You Win!");
          player.pos = 0;

          state = LEVELUP;
        }
      break;
    }

    vTaskDelay(50 / portTICK_RATE_MS);
  }
}

void ticker_cb(TimerHandle_t handler) {
  xEventGroupSetBits(count_group, 1);
}

void gpio_isr_handler(void *arg) {
  uint32_t gpio_num = (uint32_t) arg;

  xQueueSendFromISR(gpio_evt_queue, &gpio_num, NULL);
}

void gauntlet_init() {
	static struct led_color_t led_strip_buf_1[LED_COUNT];
  static struct led_color_t led_strip_buf_2[LED_COUNT];

  static struct led_strip_t led_strip = {
    .rgb_led_type = RGB_LED_TYPE_WS2812,
    .rmt_channel = RMT_CHANNEL_1,
    .rmt_interrupt_num = LED_STRIP_RMT_INTR_NUM,
    .gpio = PIN_LED,
    .led_strip_buf_1 = led_strip_buf_1,
    .led_strip_buf_2 = led_strip_buf_2,
    .led_strip_length = LED_COUNT
  };
  led_strip.access_semaphore = xSemaphoreCreateBinary();

	bool led_init_ok = led_strip_init(&led_strip);
  ESP_LOGI(TAG, "LED init okay? %s", led_init_ok ? "yes" : "no");

  count_group = xEventGroupCreate();
  count_timer = xTimerCreate("count", COUNT_TIME_MS / portTICK_RATE_MS, pdTRUE, (void *)0, ticker_cb);

  gpio_config_t io_conf;
  io_conf.intr_type = GPIO_INTR_ANYEDGE;
  io_conf.pin_bit_mask = GPIO_INPUT_PIN_SEL;
  io_conf.mode = GPIO_MODE_INPUT;
  io_conf.pull_up_en = 1;
  gpio_config(&io_conf);

  gpio_evt_queue = xQueueCreate(20, sizeof(uint32_t));

  gpio_install_isr_service(0);
  gpio_isr_handler_add(PIN_BTN1, gpio_isr_handler, (void*) PIN_BTN1);
  gpio_isr_handler_add(PIN_BTN2, gpio_isr_handler, (void*) PIN_BTN2);

  xTaskCreate(&gauntlet_loop, "gauntlet_loop",  8192, (void*)&led_strip, 6, NULL);
  xTaskCreate(&button_loop, "button_loop",  8192, NULL, 6, NULL);
}
